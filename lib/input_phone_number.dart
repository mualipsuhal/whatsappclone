import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp/choose_county.dart';

class InputPhoneNumber extends StatefulWidget {
  @override
  _InputPhoneNumberState createState() => _InputPhoneNumberState();
}

class _InputPhoneNumberState extends State<InputPhoneNumber> {
  final TextEditingController _controllerCountry = TextEditingController();
  final TextEditingController _controllerCode = TextEditingController();
  final TextEditingController _controllerPhoneNumber = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Firestore _firestore = Firestore.instance;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  List<DocumentSnapshot> _country = [];

  @override
  void initState() {
    super.initState();
    _firestore.collection('country').snapshots().listen((snapshot) {
      setState(() {
        _country = snapshot.documents;
      });
    });
    _controllerCode.addListener(() {
      String code = _controllerCode.text;
      List<DocumentSnapshot> countryFiltered = _country
          .where(
            (document) => document.data['code'] == code,
          )
          .toList();
      if (countryFiltered.length > 0)
        _controllerCountry.text = countryFiltered.first.data['name'];
      else
        _controllerCountry.text = 'Invalid country code';
      setState(() {});
    });
  }

  final PhoneCodeSent _codeSent = (String verificationId, [int forceResendingToken]) {
    print('Please check your phone for the verification code. $verificationId');
  };

  void _sendSMS() {
    _firebaseAuth.verifyPhoneNumber(
      phoneNumber: _controllerCode.text + _controllerPhoneNumber.text,
      codeAutoRetrievalTimeout: (verificationId) {},
      codeSent: _codeSent,
      timeout: Duration(seconds: 5),
      verificationCompleted: (phoneAuthCredential) {},
      verificationFailed: (error) {
        print(error.message);
      },
    );
  }

  void _submit() {
    String code = _controllerCode.text;
    String country = _controllerCountry.text;
    String phoneNumber = _controllerPhoneNumber.text;
    String warningMessange;
    if (!(code.length > 0 && code.length < 5))
      warningMessange = 'Invalid country code length (1-3 digit only)';
    else if (country == 'Invalid country code')
      warningMessange = 'Invalid country code';
    else if (phoneNumber == '') warningMessange = 'Please enter your phone number';
    if (warningMessange != null)
      showDialog(
        context: context,
        builder: (context) => SimpleDialog(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: Text(warningMessange),
            ),
          ],
        ),
      );
    else
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('We will be verifiying the phone number:'),
              SizedBox(height: 10),
              Text(
                code + ' ' + phoneNumber,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Text('Is this OK, or would you like to edit the number?')
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('EDIT'),
              onPressed: () => Navigator.pop(context),
            ),
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                _sendSMS();
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => VerifyPhoneNumber(),
                //   ),
                // );
              },
            )
          ],
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color accentColor = Theme.of(context).accentColor;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[
          PopupMenuButton(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Icon(
                Icons.more_vert,
                color: Colors.grey,
              ),
            ),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text('Help'),
              )
            ],
          )
        ],
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Verify your phone number',
          style: TextStyle(color: primaryColor),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Text(
                  'WhatsApp will send an SMS message (carrier charges may apply) to verify your phone number. Enter your country code and phone.',
                  textAlign: TextAlign.center,
                ),
                GestureDetector(
                  onTap: () async {
                    Map<String, dynamic> country = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChooseCountry(),
                      ),
                    );
                    setState(() {
                      _controllerCountry.text = country['name'];
                      _controllerCode.text = country['code'];
                    });
                  },
                  child: AbsorbPointer(
                    child: SizedBox(
                      width: 250,
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        controller: _controllerCountry,
                        decoration: InputDecoration(
                          suffixIcon: Icon(
                            Icons.arrow_drop_down,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 250,
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 50,
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          controller: _controllerCode,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          controller: _controllerPhoneNumber,
                          maxLength: 14,
                          decoration: InputDecoration(
                            counterText: "",
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(),
                ),
                RaisedButton(
                  color: accentColor,
                  onPressed: _submit,
                  child: Text(
                    'NEXT',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
