import 'package:flutter/material.dart';
import 'package:whatsapp/welcome.dart';

void main() => runApp(WhatsApp());

class WhatsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WhatsApp',
      theme: ThemeData(
        primaryColor: Colors.teal[600],
        accentColor: Colors.greenAccent[400],
      ),
      home: Welcome(),
    );
  }
}
