import 'package:flutter/material.dart';
import 'package:whatsapp/input_phone_number.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color accentColor = Theme.of(context).accentColor;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Welcome To WhatsApp',
          style: TextStyle(color: primaryColor),
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: accentColor,
                  shape: BoxShape.circle,
                ),
                height: 250,
                width: 250,
              ),
            ),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'Read our ',
                style: TextStyle(color: Colors.grey),
                children: [
                  TextSpan(
                    text: 'Privacy Policy. ',
                    style: TextStyle(color: Colors.blue),
                  ),
                  TextSpan(
                    text: 'Tap "Agree and continue" to accept the ',
                    style: TextStyle(color: Colors.grey),
                  ),
                  TextSpan(
                    text: 'Terms of Service.',
                    style: TextStyle(color: Colors.blue),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: RaisedButton(
                padding: const EdgeInsets.symmetric(horizontal: 60),
                color: accentColor,
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => InputPhoneNumber(),
                    ),
                  );
                },
                child: Text(
                  'AGREE AND CONTINUE',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
