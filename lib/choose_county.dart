import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ChooseCountry extends StatefulWidget {
  ChooseCountry({Key key}) : super(key: key);

  _ChooseCountryState createState() => _ChooseCountryState();
}

class _ChooseCountryState extends State<ChooseCountry> {
  final Firestore _firestore = Firestore.instance;
  final TextEditingController _controller = TextEditingController();
  bool _modeSearch = false;

  void _switchMode() {
    setState(() {
      _modeSearch = !_modeSearch;
    });
  }

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final Color primaryColor = Theme.of(context).primaryColor;
    return Scaffold(
      appBar: AppBar(
        title: _modeSearch
            ? TextField(
                controller: _controller,
                autofocus: true,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Search Country',
                ),
              )
            : Text(
                'Choose a country',
                style: TextStyle(color: primaryColor),
              ),
        centerTitle: false,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: primaryColor,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              _modeSearch ? Icons.close : Icons.search,
              color: primaryColor,
            ),
            onPressed: _switchMode,
          )
        ],
      ),
      body: StreamBuilder(
        stream: _firestore
            .collection('country')
            .orderBy(
              'name',
              descending: true,
            )
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<DocumentSnapshot> country = snapshot.data.documents;
            country = country
                .where(
                  (document) =>
                      document.data['name'].toLowerCase().contains(
                            _controller.text.toLowerCase(),
                          ) ||
                      document.data['code'].toLowerCase().contains(
                            _controller.text.toLowerCase(),
                          ),
                )
                .toList();
            return ListView.builder(
              itemCount: country.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, country[index].data);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.grey[300],
                        ),
                      ),
                    ),
                    child: ListTile(
                      leading: Icon(Icons.flag),
                      title: Text(country[index].data['name']),
                      subtitle: Text(country[index].data['desc']),
                      trailing: Text(
                        country[index].data['code'],
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[700],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
