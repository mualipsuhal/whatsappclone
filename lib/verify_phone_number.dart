import 'package:flutter/material.dart';

class VerifyPhoneNumber extends StatefulWidget {
  VerifyPhoneNumber({Key key}) : super(key: key);

  _VerifyPhoneNumberState createState() => _VerifyPhoneNumberState();
}

class _VerifyPhoneNumberState extends State<VerifyPhoneNumber> {
  @override
  Widget build(BuildContext context) {
    final Color primaryColor = Theme.of(context).primaryColor;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          'Verify',
          style: TextStyle(color: primaryColor),
        ),
        actions: <Widget>[
          PopupMenuButton(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Icon(
                Icons.more_vert,
                color: Colors.grey,
              ),
            ),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text('Help'),
              )
            ],
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: 'You\'ve tried to register ',
                style: TextStyle(color: Colors.black),
                children: [
                  TextSpan(
                    text: '? ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: 'recently.\nWait before requesting an SMS or call with your code',
                  ),
                ],
              ),
            ),
            SizedBox(height: 5),
            Text(
              'Wrong Number?',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.blue,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 200,
                  child: TextField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(hintText: '- - -  - - -'),
                  ),
                )
              ],
            ),
            SizedBox(height: 10),
            Text(
              'Enter 6-digit code',
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 30),
            Row(
              children: <Widget>[
                FlatButton(
                  textColor: primaryColor,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.message),
                      SizedBox(width: 10),
                      Text('Resend SMS'),
                    ],
                  ),
                  onPressed: () {},
                )
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                FlatButton(
                  textColor: primaryColor,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.call),
                      SizedBox(width: 10),
                      Text('Call me'),
                    ],
                  ),
                  onPressed: () {},
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
